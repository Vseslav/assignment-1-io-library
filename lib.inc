section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.lop:
    cmp byte [rdi+rax], 0
    je .return
    inc rax
    jmp .lop
.return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov [rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12 
    push r9
    mov r9, rsp
    mov byte[rsp], 0
    mov rax, rdi
    mov r12, 10 ; /10 -> 10 CC
    .loop:
        xor rdx, rdx 
        div r12
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        je .end
        jmp .loop               
    .end:
    mov rdi, rsp
        call print_string
    mov rsp, r9
    pop r9
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .ns ;      
    .print_number:
        push rdi
        mov rdi, 0x2D
        call print_char
        pop rdi
        neg rdi
    .ns:
        call print_uint 
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov al, byte [rdi]       
    cmp al, byte [rsi]     
    jne .not  
    inc rdi    
    inc rsi
    cmp al, 0   
    jne .loop 
    .equals:
        mov rax, 1
    ret
    .not:
        xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov [rsp], byte 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov al, [rsp]
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
    xor r14, r14
    .space_symbol:
        call read_char
        cmp rax, 0x20
        je .space_symbol
        cmp rax, 0x9
        je .space_symbol
        cmp rax, 0xA
        je .space_symbol
        cmp rax, 0
        je .end
                                            
    .read:
    cmp r13, r14
    je .err
        mov byte[r12+r14], al
        inc r14
        call read_char
        cmp rax, 0
        je .end
        cmp rax, 0x20
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end
        jmp .read
                                        
   .err:
        xor rax, rax    
        pop r14
        pop r13
        pop r12
        ret
     
    .end:
    mov byte[r12+r14], 0
        mov rax, r12
    mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rsi, rsi
    xor rax, rax

    push rbx
    mov rbx, 10;

.loop:    
    xor r10, r10
    mov r10b, byte [rdi+rsi]
    cmp r10, 0x30
    jl .return
    cmp r10, 0x39
    jg .return
    mul rbx
    add rax, r10
    sub rax, 0x30
    inc rsi
    jmp .loop
.return:
    pop rbx
    mov rdx, rsi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    push rdi
    call read_char
    pop rdi
    cmp al, 0x2D
    jz .neg
    jmp parse_uint
   .neg: 
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    ja .notgood
    .loop:
        mov r10b, byte [rdi]
        mov byte [rsi], r10b
        inc rdi
        inc rsi
    test r10b, r10b
        jz .good
        jmp .loop
    .notgood:
        mov rax, 0
        ret
    .good:
        ret
